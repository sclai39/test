﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    public class Person
    {

        public Person()
        {
            @class = "";
            family_name = "";
            first_name = "";
            profession = "";
            ward_number = "";
            staff_uuid = "";
            phone_number = "";

        }

        public string @class { get; set; }

        public string family_name { get; set; }

        public string first_name { get; set; }

        public string profession   { get; set; }

        public string ward_number { get; set; }

        public string staff_uuid { get; set; }

        public string phone_number { get; set; }





    }
}
