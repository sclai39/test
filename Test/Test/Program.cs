﻿using Test;



Parser p = new Parser("address-book.json");

var patients = p.parsePatient();
var nurses = p.parseNurses();
var doctors = p.parseDoctors();


Console.WriteLine("Patients:\n");
foreach(var patient in patients)
{

    Console.WriteLine(patient.first_name + " " + patient.family_name);

}


Console.WriteLine("\n\n\n\nNurses:\n");
String lastward = "";
foreach(var nurse in nurses)
{
    if (lastward != nurse.ward_number) {
        Console.WriteLine("\n[Ward: " + nurse.ward_number + "]\n");

    }
    lastward = nurse.ward_number;

    Console.WriteLine(nurse.first_name + " " + nurse.family_name);

}



Console.WriteLine("\n\n\n\nDoctors:\n");

foreach (var doctor in doctors)
{
   
    Console.WriteLine(doctor.first_name + " " + doctor.family_name + ": " + doctor.phone_number);

}




