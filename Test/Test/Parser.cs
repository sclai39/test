﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Test
{
    public class Parser
    {

        List<Person> Persons = new List<Person>();

        public Parser(String jsonfile)
        {

            String filecontent = File.ReadAllText(jsonfile);
            //Person person = (Person)JsonConvert.DeserializeObject(filecontent, (typeof(Person)));
            Persons = JsonConvert.DeserializeObject<IEnumerable<Person>>(filecontent).ToList();
            Console.WriteLine("here");

        }

        public List<Person> parsePatient()
        {
            
            List<Person> patientList = Persons
                .Where(s => s.@class == "Patient")
                .OrderBy(s => s.first_name).ThenBy(s => s.family_name).ToList();

            return patientList;

        }

        public List<Person> parseNurses()
        {

            List<Person> nurseList = Persons
               .Where(s => s.@class == "Nurse")
               .OrderBy(s => s.ward_number).ThenBy(s => s.first_name).ThenBy(s => s.family_name).ToList();

            return nurseList;



        }


        public List<Person> parseDoctors()
        {

            List<Person> nurseList = Persons
               .Where(s => s.@class == "Doctor")
               .OrderBy(s => s.first_name).ThenBy(s => s.family_name).ToList();

            return nurseList;



        }

    }
}
